/**
 * Search component
 */
(function(){

  var Search = {

    data: {
      list:  m.stream([]),
      detail: m.stream(null),
      userInput: m.stream(null)
    },
    ui: {
      searchListHidden: m.stream(false),
      inputValue: m.stream(null),
      showLoadingIndicator: m.stream(false)
    },
    actions: {
      fetchCountries: fetchCountries,
      setCountry: setCountry,
      formatListItem: formatListItem,
      ongoingRequest: m.stream(null)
    },

    view: function(vnode) {
      return m("div.search-ui", [
        m("h2", "Search component"),
        m("input[type='text'].search-input", {
          placeholder:"Search by country or capital",
          value: Search.ui.inputValue(),
          class: Search.ui.showLoadingIndicator() ? "loading": "",
          onclick: function(el){
            el.stopPropagation();
          },
          onkeyup: m.withAttr("value", Search.actions.fetchCountries)
        }),
        m("ul.search-list#searchList", {class: Search.ui.searchListHidden() ? "hide" : "show"}, Search.data.list().map(function(country){
          return m("li", { onclick: Search.actions.setCountry.bind(this, country)}, m.trust(Search.actions.formatListItem(country)) );
        })),
        m("div.search-detail",
          (function(){
            var detail = Search.data.detail();
            if(detail) {
              return [
                m("p", "Country: ", detail.country),
                m("p", "Capital: ", detail.capital),
                m("p", "Score: ", detail.score)
              ]
            }
          }())
        )


      ]);
    }
  }

  // Mount component
  m.mount(document.getElementById("searchUI"), Search);


  // Fetch matching countries from the server
  // Triggerd by user's on keypress
  function fetchCountries(input){
    Search.ui.showLoadingIndicator(true);
    // check for any ongoing request, abort if needed
    if (Search.actions.ongoingRequest()){
      Search.actions.ongoingRequest().abort();
    }
    Search.actions.ongoingRequest(null);
    // user is typing into the field, do not override value attr of input elem
    Search.ui.inputValue(undefined);
    Search.ui.searchListHidden(false);
    Search.data.userInput(input);
    return m.request({
      method: "GET",
      url: "https://code.totaralms.com/countries-json.php",
      data: {search: input},
      config: function(xhr) {Search.actions.ongoingRequest(xhr)}
    }).then(function(list){
      Search.ui.showLoadingIndicator(false);
      if(list.results && list.results.length > 0){
        Search.data.list(list.results);
      }
    }).catch(function(){
      Search.ui.showLoadingIndicator(false);
    });
  }

  // General UI handling when user clicks on item from the list
  function setCountry(result, el){
    el.stopPropagation();
    Search.ui.searchListHidden(true);
    Search.ui.inputValue(result.capital + ", " + result.country);
    Search.data.detail(result);
  }

  // Formats each entry in the list
  function formatListItem(result){
    var city = result.capital;
    var country = result.country;
    var userInputRegExp = new RegExp(Search.data.userInput(), "i");
    // wrap matched text with bold tag
    if(city.search(userInputRegExp) > -1){
      city = city.replace(userInputRegExp, "<strong>" + city.match(userInputRegExp)  + "</strong>");
      // check if uppercase for first letter in a city is needed
      if(city.charAt(0) === "<"){
        city = "<strong>" + city.charAt( city.indexOf(">")+1 ).toUpperCase() + city.slice( city.indexOf(">")+2  );
      }
    } else if(country.search(userInputRegExp) > -1){
      country = country.replace(userInputRegExp, "<strong>" + country.match(userInputRegExp) + "</strong>");
      // check if uppercase for first letter in a country is needed
      if(country.charAt(0) === "<"){
        country = "<strong>" + country.charAt(  country.indexOf(">")+1 ).toUpperCase() + country.slice(  country.indexOf(">")+2  );
      }
    }
    return city + ", " + country;
  }


  document.addEventListener("click", function(){
    if(Search.ui.searchListHidden() === false){
      document.getElementById("searchList").classList.replace("show", "hide");
      Search.ui.searchListHidden(true);
    }
  });

}())
