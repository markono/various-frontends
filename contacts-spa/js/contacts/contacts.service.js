(function(){
  'use-strict';


  angular
      .module('contacts')
      .factory('contactsService', contactsService);

  contactsService.$inject = ['$http', '$q'];

  function contactsService($http, $q) {
      return {
          fetchContacts: fetchContacts,
          fetchContactsById: fetchContactsById
      };


      function fetchContacts() {
          var q = $q.defer();

          httpRequest(success, failed);
          // callbacks:
          function success(response) {
            q.resolve(response);
          }
          function failed(error) {
            q.reject(error);
          }
          return q.promise;
      }


      function fetchContactsById(id) {
        var q = $q.defer();

        httpRequest(success, failed);
        // callbacks:
        function success(response) {
          q.resolve(response.data.filter(function(obj){
            return id === obj.id;
          })[0]);
        }
        function failed(error) {
          q.reject(error);
        }
        return q.promise;
      }


      function httpRequest(success, failed){
        $http({
          method: 'GET',
          url: 'http://jsonplaceholder.typicode.com/users',
          timeout: 10000
        }).then(success).catch(failed);
      }
  }


}());
