(function(){
  'use-strict';

  angular
      .module('contacts.details')
      .controller('DetailsController', DetailsController);

  DetailsController.inject = ["$rootScope","$timeout", "contactsService", "$rootScope", "$stateParams"];

  function DetailsController($rootScope, $timeout, contactsService, $rootScope, $stateParams) {
    var vm = this;
    var id = $stateParams.id;
    vm.contact = {};
    $rootScope.page = "details";

    if(id){
      contactsService.fetchContactsById( parseInt(id) ).then(function(response){
        vm.contact = response;
      }).catch(function(response){
        vm.contact = {};
      }).finally(function(){
        $rootScope.loading = false;
        // wait for fade to finish
        // so we can hide the loader
        $timeout(function(){
          $rootScope.hideLoader = true;
        }, 500);
      });
    }

  }


}());
