(function(){
  'use-strict';

  angular
      .module('contacts.details')
      .config(config);

  config.inject = ["$stateProvider"];

  function config($stateProvider) {
      $stateProvider
          .state('contacts.details', {
              url: "/details/:id",
              templateUrl: 'js/contacts/details/details.html',
              controller: 'DetailsController',
              controllerAs: 'vm'
          });
  }


}());
