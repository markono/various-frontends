describe('DetailsController: ', function() {
  beforeEach(module('contacts'));

  var $controller, $httpBackend, $scope ,$stateParams;

  beforeEach(inject(function(_$controller_, _$httpBackend_,_$rootScope_,_$stateParams_){
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    $scope = _$rootScope_.$new();
    $stateParams = _$stateParams_;
  }));


  it('should find person with id 2', function() {
    // expected data
    var finalData = { name: "mina Uxe", id: 2 };
    // initiate controller, provide id in stateParams
    var controller = $controller('DetailsController', { $stateParams: {id: 2} });
    $httpBackend.expectGET("http://jsonplaceholder.typicode.com/users").respond([
      // data passed to mocked response
      {name: "Mick Obernauer", id: 1}, {name: "mina Uxe", id: 2}, {name: "Alex Wayne", id: 3}
    ]);
    $scope.$apply();
    $httpBackend.flush();
    expect(controller.contact).toEqual(finalData);
  });


  it('no person should be found', function() {
      var finalData = {};
      var controller = $controller('DetailsController', { $stateParams: {id: null} });
      $scope.$apply();
      expect(controller.contact).toEqual(finalData);
    });

});
