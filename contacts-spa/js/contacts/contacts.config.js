(function(){
  'use-strict';

  angular
      .module('contacts')
      .config(config)
      .run(run);

  config.inject = ["$stateProvider", "$urlRouterProvider"];
  run.inject = ["$rootScope", "$state"];


  function run($rootScope, $state) {
    $rootScope.$on('$stateChangeStart', function(event, toState, fromState) {
        $rootScope.loading = true;
        $rootScope.hideLoader = false;
        if(toState.url === "/contacts"){
          event.preventDefault();
          $state.transitionTo("contacts.list");
        }
    })
  }

  function config($stateProvider, $urlRouterProvider) {

      $urlRouterProvider.otherwise('/contacts/list');

      $stateProvider
          .state('contacts', {
              url: "/contacts",
              templateUrl: 'js/contacts/contacts.html'
          });




  }


}());
