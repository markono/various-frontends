
describe('Fetching contacts in contactsService:', function () {
    var contactsService, $httpBackend;
    beforeEach(module("contacts"));
    beforeEach(function () {
        inject(function (_$httpBackend_, _contactsService_) {
            contactsService = _contactsService_;
            $httpBackend = _$httpBackend_;
        });
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('fetched contact should be contact with id of 1', function () {
        var contact;
        var finalData = [{id: 1}];
        $httpBackend.expectGET("http://jsonplaceholder.typicode.com/users").respond(finalData);
        contactsService.fetchContactsById(1).then(function (response) {
          contact = response;
        });
        $httpBackend.flush();
        expect(contact.id).toEqual(finalData[0].id);

    });

    it('fetched contacts should be array', function () {
        var contacts;
        var finalData = [];
        $httpBackend.expectGET("http://jsonplaceholder.typicode.com/users").respond(finalData);
        contactsService.fetchContacts().then(function (response) {
          contacts = response.data;
        });
        $httpBackend.flush();
        expect(Array.isArray(contacts)).toEqual(Array.isArray(finalData));

    });

});
