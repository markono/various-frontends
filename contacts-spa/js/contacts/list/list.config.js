(function(){
  'use-strict';

  // route-config.js
  angular
      .module('contacts.list')
      .config(config);

  config.inject = ["$stateProvider"];

  function config($stateProvider) {
      $stateProvider
          .state('contacts.list', {
              url: "/list",
              templateUrl: 'js/contacts/list/list.html',
              controller: 'ListController',
              controllerAs: 'vm'
          });
  }


}());
