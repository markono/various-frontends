describe('ListController: ', function() {
  beforeEach(module('contacts'));

  var $controller, $httpBackend, $scope;

  beforeEach(inject(function(_$controller_, _$httpBackend_,_$rootScope_){
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    $scope = _$rootScope_.$new();
  }));

  it('should print 3 persons sorted by name ascending', function() {
    var finalData = [{name: "Alex Wayne"},{name: "Mick Obernauer"}, {name: "mina Uxe"}, ];
    var controller = $controller('ListController', {});
    $httpBackend.expectGET("http://jsonplaceholder.typicode.com/users").respond([
      {name: "Mick Obernauer"}, {name: "mina Uxe"}, {name: "Alex Wayne"}
    ]);
    $scope.$apply();
    $httpBackend.flush();
    expect(controller.contactList).toEqual(finalData);
  });

  it('should print 3 persons sorted by name descending', function() {
      var finalData = [{name: "mina Uxe"}, {name: "Mick Obernauer"},{name: "Alex Wayne"}, ];
      var controller = $controller('ListController', {});
      controller.reverse = true;
      $httpBackend.expectGET("http://jsonplaceholder.typicode.com/users").respond([
        {name: "Alex Wayne"}, {name: "Mick Obernauer"}, {name: "mina Uxe"}
      ]);
      $scope.$apply();
      $httpBackend.flush();
      expect(controller.contactList).toEqual(finalData);
    });

});
