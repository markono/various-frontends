(function(){
  'use-strict';

  angular
      .module('contacts.list')
      .controller('ListController', ListController);

  ListController.inject = ["$rootScope","$timeout", "contactsService", "$rootScope", "orderByFilter"];

  function ListController($rootScope, $timeout, contactsService, $rootScope, orderByFilter) {
    var vm = this;
    var propertyName = "name";
    vm.contactList = [];
    vm.reverse = false;
    vm.changeOrder = changeOrder;
    vm.searchByName = "";

    $rootScope.page = "contacts";

    // fetch contacts
    contactsService.fetchContacts().then(function(response){
      if(response.data && response.data.length > 0){
        vm.contactList = orderByFilter(response.data, propertyName, vm.reverse);
      }
    }).catch(function(response){
      vm.contactList = [];
    }).finally(function(){
      $rootScope.loading = false;
      // wait for fade to finish
      // so we can hide the loader
      $timeout(function(){
        $rootScope.hideLoader = true;
      }, 500);
    });

    function changeOrder(){
      vm.reverse = !vm.reverse;
      vm.contactList = orderByFilter(vm.contactList, propertyName, vm.reverse);
    }
  }


}());
