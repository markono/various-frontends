(function(){
  'use-strict';

  angular.module('contacts', ['ui.router', 'contacts.report', 'contacts.list', 'contacts.details']);

}());
