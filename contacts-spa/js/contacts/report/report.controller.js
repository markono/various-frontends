(function(){
  'use-strict';

  angular
      .module('contacts.report')
      .controller('ReportController', ReportController);

  ReportController.inject = ["$rootScope", "$timeout", "contactsService"];

  function ReportController($rootScope, $timeout, contactsService) {
    var vm = this;
    vm.contactList = [];
    $rootScope.page = "report";

    // fetch contacts
    contactsService.fetchContacts().then(function(response){
      if(response.data && response.data.length > 0){
        createAlphabetArray(response.data);
      }
    }).catch(function(response){
      vm.contactList = [];
    }).finally(function(){
      $rootScope.loading = false;
      // wait for fade to finish
      // so we can hide the loader
      $timeout(function(){
        $rootScope.hideLoader = true;
      }, 500);
    });

    function createAlphabetArray(contacts){
      contacts.forEach(function(contact){
        var exist = false;
        var firstCharacter = contact.name.substring(0,1).toUpperCase();

        vm.contactList.forEach(function(characterObject){
          if(characterObject.character === firstCharacter){
            characterObject.count++;
            exist = true;
            return;
          }
        });

        if(!exist){
          var characterGroup = {};
          characterGroup.character = firstCharacter;
          characterGroup.count = 1;
          vm.contactList.push(characterGroup);
        }
      })

    }

  }


}());
