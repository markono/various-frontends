(function(){
  'use-strict';

  // route-config.js
  angular
      .module('contacts.report')
      .config(config);

  config.inject = ["$stateProvider"];

  function config($stateProvider) {
      $stateProvider
          .state('contacts.report', {
              url: "/report",
              templateUrl: 'js/contacts/report/report.html',
              controller: 'ReportController',
              controllerAs: 'vm'
          });
  }


}());
