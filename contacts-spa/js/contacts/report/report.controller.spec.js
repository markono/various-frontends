describe('ReportController: ', function() {
  beforeEach(module('contacts'));

  var $controller, $httpBackend, $scope;

  beforeEach(inject(function(_$controller_, _$httpBackend_,_$rootScope_){
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    $scope = _$rootScope_.$new();
  }));


  it('should print two persons that have capital M as a first letter of a name', function() {
    var finalData = [{character: "M", count: 2}];
    var controller = $controller('ReportController', {});
    $httpBackend.expectGET("http://jsonplaceholder.typicode.com/users").respond([
      {name: "Mick Obernauer"}, {name: "mina Uxe"}
    ]);
    $scope.$apply();
    $httpBackend.flush();
    expect(controller.contactList).toEqual(finalData);
  });

});
