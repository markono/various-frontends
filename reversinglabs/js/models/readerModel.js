define([
  'underscore',
  'backbone'
  ], function(_, Backbone){
	return Backbone.Model.extend({
	    urlRoot: "http://reversinglabs.bergb.com/reader.php",
		parse: function(response) {
			if(_.isEmpty(response))
				return [];  
				
			return (!_.isArray(arguments[0])) ? {Blob: response} : response;
		} 
	}); 
});
