require.config({
	urlArgs: "v=1.3",
	paths: {
	    jquery: 'lib/jquery',
	    backbone: 'lib/backbone.min', 
	    underscore: 'lib/underscore.min',
	    text: 'lib/require/text',
	    binary: 'lib/binary'	
	},
	shim: {
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },		
		underscore: {
            exports: '_'
        }
    }  
});


require(['router','backbone'], function(Router,Backbone) {
	Router.initialize();
});
