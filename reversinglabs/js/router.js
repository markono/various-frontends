define([
  'jquery',
  'underscore',
  'backbone',
  'views/readerView'
], function($, _, Backbone, ReaderView){
	var Router = Backbone.Router.extend({
		mainView: null,
		routes: {
			'': 'index',
			':mod': 'index'
		},

	  	index: function(){
			if(!this.mainView){
				this.mainView = new ReaderView();
			} else {
				// got view, open reader  
				this.mainView.openReaderViewWithFragment();	
			}
					
		}	
	});
	
 	var initialize = function(){ 
		var router = new Router(); 
		
		Backbone.history.start({
			pushState: true, 
			hashChange: false,
			root: "/reversinglabs/"
		});		
	};
	
  return {
    initialize: initialize
  };
});