define([
  'jquery',
  'underscore',
  'backbone',
  'binary',
  'models/readerModel',
  'text!templates/readerHtmlListing.html',
  'text!templates/readerHtmlView.html'
], function($, _, Backbone, binary, ReaderModel, readerListing, readerView){

	return Backbone.View.extend({	
		el: "#container",
		currentViewName: null,
		currentViewSize: null,
		chunks: 1023,
		currentChunks: 0,
		$loader: $("body").find(".loader"),
		headingInfo: "<span class='info'>Use your mouse wheel inside this viewing area to list through pages </span>",
		searchBox:	"<div id='searchBox'>" +
						"<input type='text' placeholder='Search for a value'/><button>Go</button>" +
						"<label><span>Hex</span><input type='radio' name='type' value='hex' /></label>" +
						"<label><span>Char</span><input type='radio' name='type' value='char' checked/></label>" +
					"</div>",

		events: { 
	        'click a': 'openReaderView',
			'mousewheel #readerView': "showAdditionalChunks",
			'mouseover .hoverable': "hoverElements",
			'mouseleave .hoverable': "unHoverElements",
			'click #searchBox button': "searchForElements",
			'keypress #searchBox input[type="text"]': function(e){
				if(e.keyCode === 13){
					this.searchForElements(e);
				}
			},
			
	    },	
		
		initialize: function(data){		
			this.$loader.show();		
			this.model = new ReaderModel();				
			this.model.fetch();	 
			this.listenToOnce(this.model, 'sync', _.bind(this.renderListing, this));	
		},	
		
		renderListing: function() {		
			if(!_.isEmpty(this.model.attributes)){				
				this.$loader.hide();				
				var compiledTemplate = _.template(readerListing, { 
					list: this.model.attributes
				},{variable: 'obj'});
				this.$el.find("#readerListing").html(compiledTemplate);	
				
				// url check for fragments
				if(Backbone.history.fragment){ 
					this.openReaderViewWithFragment();	
				}
			}				
			this.listenTo(this.model, 'sync', this.renderView);
	    },
 
		renderView: function() {
			var that = this;	
			if(!_.isEmpty(this.model.attributes.Blob)){				
				this.$loader.hide();			
				this.$el.find("#reader").removeClass("loader-inline");				
				var fileReader = new FileReader();
				fileReader.onload = function() {
					var _uint = new Uint8Array(this.result);
					var _characters = String.fromCharCode.apply(null,_uint);
					
					var compiledTemplate = _.template(readerView, { 
						title: that.currentViewName,
						uint: _uint,					
						characters: _characters.split(''),
						currentRow: Math.ceil(that.currentChunks/16)
					},{variable: 'obj'});	
					
					
					that.$el.find("#reader").html(compiledTemplate);							
					that.$el.find("h2").html(that.currentViewName + 
						"<span class='pages'> Page " + parseInt(that.currentChunks/that.chunks + 1) + " of " + 
							Math.ceil(that.currentViewSize/that.chunks) + "</span>" + that.headingInfo);
							
					that.$el.find("#searchBox").html(that.searchBox);
					
				};				
				fileReader.readAsArrayBuffer(this.model.attributes.Blob);	
			}	
	    },
		
		openReaderView: function(e) {		
			e.preventDefault();
			
			var $e = $(e.currentTarget);
			this.currentViewName = $e.data("name");
			this.currentViewSize = $e.data("size");
			
			this.fetchPage();

		},	
		
		openReaderViewWithFragment: function() {

			this.currentViewName = Backbone.history.fragment;
			this.currentViewSize = $("#readerListing li").find("[href='" + this.currentViewName + "']").data("size");
			
			this.fetchPage();
		},
		
		fetchPage: function() {	
		
			this.$loader.show();
			this.currentChunks = 0;
			this.model.fetch({ 
				dataType : 'binary',
				traditional: true,				
				data: {
					filename: this.currentViewName,
					seek: 0,
					size: 1024
				}
			});	
			
			// save url to history, but don't trigger router
			Backbone.history.navigate('/' + this.currentViewName, {trigger: false, replace: false}); 
		},
			
			
		showAdditionalChunks: function(e) {	
			e.preventDefault();		
			if(this.currentChunks === 0 && e.originalEvent.wheelDelta > 0){
				return;
			} else if(this.currentChunks >= this.currentViewSize){
				console.log("No no, no more pages.");
				this.currentChunks -= this.chunks;
				return;
			}
			
			//console.log(this.currentChunks, '>=', this.currentViewSize)
			if(e.originalEvent.wheelDelta < 0){
				this.currentChunks += this.chunks;
			} else {
				this.currentChunks -= this.chunks;
			}
			
			if(this.currentChunks >= this.currentViewSize){
				console.log("Sorry, no more pages.");
				return;
			}	 
			
			// add loader only to viewing area of doc.
			var obj = this.$el.find("#reader").addClass("loader-inline");
			obj.children(".reader-container").children("span").width(obj.children(".reader-container").width());
	
			this.model.fetch({ 
				dataType : 'binary',
				traditional: true,				
				data: {
					filename: this.currentViewName,
					seek: this.currentChunks,
					size: 1024
				}
			});			
		},
		
		hoverElements: function(e) {
			var $currentElem = $(e.currentTarget), $otherElem;
			var selector = $currentElem.data('selector');
			if($currentElem.hasClass("bytes")){
				$otherElem = $(".chars[data-selector='" + selector + "']");
			} else {
				$otherElem = $(".bytes[data-selector='" + selector + "']");
			}		
			$otherElem.addClass("hover");	
		},
		
		unHoverElements: function(e) {
			var $currentElem = $(e.currentTarget), $otherElem;
			var selector = $currentElem.data('selector');
			if($currentElem.hasClass("bytes")){
				$otherElem = $(".chars[data-selector='" + selector + "']");
			} else {
				$otherElem = $(".bytes[data-selector='" + selector + "']");
			}		
			$otherElem.removeClass("hover");	
		},
		
		searchForElements: function(e) {
			e.preventDefault();
			
			var radioValue = $("#searchBox input[type=radio]:checked").val(), $elems, $possibleMatches;
			var inputValue = $("#searchBox input[type=text]").val();
			
			if(radioValue === "hex"){
				$elems = $(".bytes");
				$(".hoverable").removeClass("hover");
				$elems.each(function(){
					var $that = $(this);
					if($that.text() === inputValue.toLowerCase()){
						$that.addClass("hover");
					}
				});						
			} else {
				$(".hoverable").removeClass("hover");
				
				// save elements if they match the beggining of search input value 
				$possibleMatches = $(".chars").filter(function(index) {
					return $(this).text().match(new RegExp('^' + inputValue[0]));				
				});
				
				// iterate thru that possibleMatches so we can find their next dom elements and if thay match next search input character
				$possibleMatches.each(function(i){
					var $currentElem = $(this);
					var $nextElement = $currentElem;
					
					// push first matched element into array
					var matchedElements = [];
					matchedElements.push($nextElement);
					// remove that first search input character since it's matched element is already saved into array
					var inputArray = inputValue.split('');
					inputArray.shift();
					
					$.each(inputArray, function(n,s){					
						function get($obj){	
							if($obj.text() === s){
								return $obj;	
							}
						};							
						if($nextElement){
							// fetch and match next dom element
							$nextElement = get($nextElement.next());
							if($nextElement && $nextElement.length > 0){
								matchedElements.push($nextElement);	
							}
						}
					});
					
					if(matchedElements.length === inputValue.length){
						$.each(matchedElements, function(){	
							$(this).addClass("hover");
						});							
					}

				});
			}
		}
		
	});
});

