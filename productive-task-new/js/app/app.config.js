(function(){
  'use-strict';

  /**
   * Starting route for the app.
   * Fallback route is login-in screen.
   *
   */

  angular
      .module('app')
      .config(config)
      .run(run);

  config.inject = ["$stateProvider", "$urlRouterProvider"];
  run.inject = ["$rootScope", "$state"];

  function run($rootScope, $state) {
    $rootScope.$on('$stateChangeStart', function(event, toState, fromState) {
        $rootScope.loading = true;
        $rootScope.hideLoader = false;
    })
  }

  function config($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise('/app/login');
      $stateProvider
          .state('app', {
            url: "/app",
            templateUrl: 'js/app/app.html'
        });
  }

}());
