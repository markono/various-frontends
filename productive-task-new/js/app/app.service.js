(function(){
  'use-strict';


  angular
      .module('app')
      .factory('appService', appService);

  function appService() {

      // hardcoded for demo purposes
      // values can be changed via login form
      var companyId = "1494";
      var userToken = "d082b2ad-6e51-4c0e-9398-9f91dc4fc7d9";

      return {
          getCompanyId: getCompanyId,
          getUserToken: getUserToken,
          setCompanyId: setCompanyId,
          setUserToken: setUserToken
      };


      ///////////////////////////////
      ///// METHOD DEFINITIONS //////
      ///////////////////////////////

      function getCompanyId() {
          return companyId;
      }

      function setCompanyId( id ) {
          companyId = id;
      }

      function getUserToken() {
          return userToken;
      }

      function setUserToken( token ) {
          userToken = token;
      }

  }


}());
