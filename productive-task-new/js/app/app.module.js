(function(){
  'use-strict';

  /**
   * App module.
   * Include Angular Router as dependency.
   *
   */

  angular.module('app', ['ui.router']);

}());
