(function(){
  'use-strict';

  /**
   * Constant variables used across the app.
   *
   */

  angular
      .module('app')
      .constant('AppConstants', AppConstants);

  function AppConstants() {
      return {
        api: {
          memberships: "https://api.productive.io/api/v2/organization_memberships/",
          time_entries: "https://api.productive.io/api/v2/time_entries",
          services: "https://api.productive.io/api/v2/services",
          tasks: "https://api.productive.io/api/v2/tasks"
        }
    }
  }
}());
