describe('LoginController: ', function() {
  beforeEach(module('app.login'));

  var $controller, $scope, appService, $state;

  beforeEach(inject(function(_$controller_,_$rootScope_,_appService_, _$state_){
    $controller = _$controller_;
    $scope = _$rootScope_.$new();
    appService = _appService_;
    $state = _$state_;
  }));


  it("Should find company id and user token available", function() {
    var controller = $controller('LoginController');
    var companyId = appService.getCompanyId();
    var userToken = appService.getUserToken();
    $scope.$apply();
    expect(controller.companyId).toEqual(companyId);
    expect(controller.userToken).toEqual(userToken);
  });


  it('Should be able to change initial company id and user token values after authorize action', function() {
    var controller = $controller('LoginController');
    spyOn($state, 'go');
    controller.companyId = "100";
    controller.userToken = "dd12-01ddd-www12";
    controller.authorize(document.createEvent("MouseEvent"));
    $scope.$apply();
    expect(controller.companyId).toEqual(appService.getCompanyId());
    expect(controller.userToken).toEqual(appService.getUserToken());
    expect($state.go).toHaveBeenCalledWith("app.timetracker");
  });


  it('Error should be presented and timetracker screen should not be visible if credentials are missing', function() {
    var controller = $controller('LoginController');
    spyOn($state, 'go');
    controller.companyId = "";
    controller.userToken = "dd12-01ddd-www12";
    controller.authorize(document.createEvent("MouseEvent"));
    $scope.$apply();
    expect(controller.error).toEqual("Please provide required data.");
    expect($state.go).not.toHaveBeenCalledWith("app.timetracker");
  });


});
