(function(){
  'use-strict';

  /**
   * Login route for the app.
   * It doesn't serve any real purpose here (since there is no actual login procedure),
   * only eye candy :)
   *
   */

  angular
      .module('app.login')
      .config(config);

  config.inject = ["$stateProvider"];

  function config($stateProvider) {
      $stateProvider
          .state('app.login', {
              url: "/login",
              templateUrl: 'js/app/login/login.html',
              controller: 'LoginController',
              controllerAs: 'vm'
          });
  }

}());
