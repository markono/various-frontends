(function(){
  'use-strict';

  /**
   * Controller for Login screen.
   *
   */

  angular
      .module('app.login')
      .controller('LoginController', LoginController);

  LoginController.inject = ["$rootScope", "$timeout", "$state", "appService"];

  function LoginController($rootScope, $timeout, $state, appService) {
    var vm = this;

    // ViewModel data
    vm.companyId = appService.getCompanyId();
    vm.userToken = appService.getUserToken();
    vm.authorize = authorize;
    vm.dismiss = dismiss;
    vm.dismissed = false;
    vm.hiddenDismissable = false;
    vm.error = false;

    // Loader indicator "global" variable
    $rootScope.loading = false;
    $timeout(function(){
      $rootScope.hideLoader = true;
    }, 500);



    ///////////////////////////////
    ///// METHOD DEFINITIONS //////
    ///////////////////////////////

    function authorize($event){
      $event.preventDefault();
      if( !vm.companyId || !vm.userToken ){
        vm.error = "Please provide required data.";
        vm.hiddenDismissable = false;
        vm.dismissed = false;
        return;
      }
      appService.setCompanyId( vm.companyId );
      appService.setUserToken( vm.userToken );
      $state.go("app.timetracker");
    }


    function dismiss(){
      vm.dismissed = true;
      $timeout(function(){
        vm.hiddenDismissable = true;
      }, 500);
    }

  }


}());
