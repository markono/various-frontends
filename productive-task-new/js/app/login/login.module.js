(function(){
  'use-strict';

  /**
   * Login module.
   * Include Timetracker module as a dependency.
   *
   */

  angular.module('app.login', [ 'app', 'app.timetracker']);

}());
