describe('appService:', function () {
    var appService, $scope;
    beforeEach(module("app"));
    beforeEach(function () {
        inject(function (_appService_, _$rootScope_) {
            appService = _appService_;
            $scope = _$rootScope_.$new();
        });
    });

    it('Company id and user token values should have default values', function () {
        expect(appService.getCompanyId()).not.toBeUndefined();
        expect(appService.getUserToken()).not.toBeUndefined();
    });

    it('Company id and user token values can be changed', function () {
        appService.setCompanyId("100");
        appService.setUserToken("dd12-01ddd-www12");
        $scope.$apply();
        expect(appService.getCompanyId()).toBe("100");
        expect(appService.getUserToken()).toBe("dd12-01ddd-www12");
    });

});
