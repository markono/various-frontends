describe('timetrackerService:', function () {
    var appConstants,timetrackerService, $httpBackend;
    beforeEach(module("app.timetracker"));
    beforeEach(function () {
        inject(function (_$httpBackend_, _AppConstants_,_timetrackerService_) {
            appConstants = _AppConstants_();
            timetrackerService = _timetrackerService_;
            $httpBackend = _$httpBackend_;
        });
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    it('Organization memberships should be fetched with custom headers and fetched item id should be 1', function () {
        var data;
        var finalData = [{id: 1}];
        $httpBackend.expectGET(appConstants.api.memberships).respond(finalData);
        timetrackerService.fetchOrganizationMemberships({"custom":"xyz"}).then(function (response) {
          data = response;
        });
        $httpBackend.flush();
        expect(data.data[0].id).toEqual(finalData[0].id);
        expect(data.config.headers.custom).toEqual("xyz");
    });


    it('Organization memberships should not be fetched due to Internal Server Error', function () {
        var data;
        var finalData = [{status: 500}];
        $httpBackend.expectGET(appConstants.api.memberships).respond(500, finalData);
        timetrackerService.fetchOrganizationMemberships(1).catch(function (response) {
          data = response;
        });
        $httpBackend.flush();
        expect(data.status).toEqual(finalData[0].status);

    });


    it('TimeEntries should be fetched with custom headers and fetched item id should be 1', function () {
        var data;
        var finalData = [{id: 1}];
        $httpBackend.expectGET(appConstants.api.time_entries + "?filter%5Bafter%5D=2018-03-15&filter%5Bbefore%5D=2018-03-18&filter%5Bperson_id%5D=1").respond(finalData);
        timetrackerService.fetchTimeEntriesByFilters(
          {"custom":"xyz"},
          {personId: 1, after: "2018-03-15", before: "2018-03-18"}
        ).then(function (response) {
          data = response;
        });
        $httpBackend.flush();
        expect(data.data[0].id).toEqual(finalData[0].id);
        expect(data.config.headers.custom).toEqual("xyz");
    });



    it('Organization memberships should not be fetched due to Internal Server Error', function () {
        var data;
        var finalData = [{status: 500}];
        $httpBackend.expectGET(appConstants.api.time_entries + "?filter%5Bafter%5D=2018-03-15&filter%5Bbefore%5D=2018-03-18&filter%5Bperson_id%5D=1").respond(500, finalData);
        timetrackerService.fetchTimeEntriesByFilters(
          {"custom":"xyz"},
          {personId: 1, after: "2018-03-15", before: "2018-03-18"}
        ).catch(function (response) {
          data = response;
        });
        $httpBackend.flush();
        expect(data.status).toEqual(finalData[0].status);

    });



    it('Services should be fetched with custom headers and fetched item id should be 1', function () {
        var data;
        var finalData = [{id: 1}];
        $httpBackend.expectGET(appConstants.api.services).respond(finalData);
        timetrackerService.fetchServices({"custom":"xyz"}).then(function (response) {
          data = response;
        });
        $httpBackend.flush();
        expect(data.data[0].id).toEqual(finalData[0].id);
        expect(data.config.headers.custom).toEqual("xyz");
    });


    it('Services should not be fetched due to Internal Server Error', function () {
        var data;
        var finalData = [{status: 500}];
        $httpBackend.expectGET(appConstants.api.services).respond(500, finalData);
        timetrackerService.fetchServices(1).catch(function (response) {
          data = response;
        });
        $httpBackend.flush();
        expect(data.status).toEqual(finalData[0].status);

    });


    it('TimeEntry should be created with "service: 1" data and custom headers and header content-type "application/vnd.api+json" should be  set while requesting', function () {
        var data;
        var finalData = [{service: 1}];
        $httpBackend.expectPOST(appConstants.api.time_entries).respond(finalData);
        timetrackerService.createTimeEntry({"custom":"xyz"}, {service: 1}).then(function (response) {
          data = response;
        });
        $httpBackend.flush();
        expect(data.config.headers.custom).toEqual("xyz");
        expect(data.config.data.service).toEqual(1);
    });



    it('TimeEntry should not be created due to Internal Server Error', function () {
        var data;
        var finalData = [{status: 500}];
        $httpBackend.expectPOST(appConstants.api.time_entries).respond(500, finalData);
        timetrackerService.createTimeEntry({"custom":"xyz"}, {service: 1}).catch(function (response) {
          data = response;
        });
        $httpBackend.flush();
        expect(data.status).toEqual(finalData[0].status);

    });



    it('TimeEntry with "id: 1" should be deleted and custom headers should be set while requesting', function () {
        var data;
        var finalData = [{service: 1}];
        $httpBackend.expectDELETE(appConstants.api.time_entries + "/" + 1).respond(finalData);
        timetrackerService.deleteTimeEntry({"custom":"xyz"}, 1).then(function (response) {
          data = response;
        });
        $httpBackend.flush();
        expect(data.config.headers.custom).toEqual("xyz");
        expect(data.status).toEqual(200);
    });



    it('TimeEntry with "id: 1" should not be deleted due to Internal Server Error', function () {
        var data;
        var finalData = [{service: 1}];
        $httpBackend.expectDELETE(appConstants.api.time_entries + "/" + 1).respond(500, finalData);
        timetrackerService.deleteTimeEntry({"custom":"xyz"}, 1).catch(function (response) {
          data = response;
        });
        $httpBackend.flush();
        expect(data.status).toEqual(500);
    });


});
