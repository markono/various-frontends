(function(){
  'use-strict';

  /**
   * Controller for Timetracker screen.
   *
   */

  angular
      .module('app.timetracker')
      .controller('TimetrackerController', TimetrackerController);

  TimetrackerController.inject = ["$q", "$rootScope", "$timeout","timetrackerService", "appService", "headers"];

  function TimetrackerController($q, $rootScope, $timeout, timetrackerService, appService, headers) {
    var vm = this
    var personId;

    // ViewModel data
    vm.timeEntries = [];
    vm.today = null;
    vm.visible = false;
    vm.time = null;
    vm.errors = {};
    vm.service = {};
    vm.deleteTimeEntry = deleteTimeEntry;
    vm.createTimeEntry = createTimeEntry;
    vm.anyErrors = anyErrors;
    vm.dismiss = dismiss;
    vm.dismissed = false;
    vm.hiddenDismissable = false;
    vm.hideTimetrackerLoader = true;
    vm.loadingTimetracker = false;

    // exposed for testing only
    vm.formatDate = formatDate;
    vm.substractDaysFromDate = substractDaysFromDate;;
    vm.addDaysToDate = addDaysToDate;


    initPage();

    ///////////////////////////////
    ///// METHOD DEFINITIONS //////
    ///////////////////////////////

    /**
     * Add timetracker list to the page and
     * resolve services for creating new entry section.
     * Once both actions are completed (successfully or not), remove loading indicator from the page.
     */
    function initPage(){
      $q.all([
        renderTimetrackList(),
        renderCreateNewEntry()
      ]).then(function(){
        hideLoaderIndicator();
        vm.visible = true;
      }).catch(function(){
        hideLoaderIndicator();
        vm.visible = true;
      });
    }



    /**
     * Render timeEntries onto the page or present an error.
     *
     */
    function renderTimetrackList(){
      var q = $q.defer();

      timetrackerService.fetchOrganizationMemberships(headers).then(function(organizationMemberships){
        if(vm.errors.timeEntries && vm.errors.timeEntries.length > 0) {
          vm.errors.timeEntries = [];
        }
        resolveTimeEntries( organizationMemberships ).then(function( response ){
          if(vm.errors.timeEntries && vm.errors.timeEntries.length > 0) {
            vm.errors.timeEntries = [];
          }
          vm.timeEntries = response.data.data;
          q.resolve( true );
        }).catch(function(error){
          if(error && error.data && error.data.errors.length > 0){
            vm.errors.timeEntries = error.data.errors;
          }
          q.reject();
        });

      }).catch(function(error){
        if(error && error.data && error.data.errors.length > 0){
          vm.errors.timeEntries = error.data.errors;
        }
        q.reject();
      });
      return q.promise;
    }


    /**
     * Render section for creating new entry or present an error.
     *
     */
     function renderCreateNewEntry(){
       var q = $q.defer();
       timetrackerService.fetchServices(headers).then(function(services){
         if(vm.errors.services && vm.errors.services.length > 0) {
           vm.errors.services = [];
         }
         vm.service = services.data.data[0];
         q.resolve( true );
       }).catch(function(error){
         if(error && error.data && error.data.errors.length > 0){
           vm.errors.services = error.data.errors;
         }
         q.reject();
       });
       return q.promise;

    }


    /*
     * Prepares data for fetching time entries from the service layer and returns
     * a promise for subsequent operations.
     *
     */
    function resolveTimeEntries( organizationMemberships ){

      var q = $q.defer();
      var today = new Date();
      var before = formatDate( addDaysToDate( 1, today ) );
      var after = formatDate( substractDaysFromDate( 1, today ) );
      var person;
      vm.today = formatDate(today);

      personId = findPersonId( organizationMemberships.data.data );
      if( typeof personId !== "undefined" ){
        person = findPersonById( personId, organizationMemberships.data.included );
        if( typeof person !== "undefined" && before && after ){
          // We have all required data
          // Fetch and render timetrack entries onto page
          timetrackerService.fetchTimeEntriesByFilters(headers, {
              personId: personId,
              before: before,
              after: after
          }).then(function(response){
            q.resolve( response );
          }).catch(function(error){
            q.reject( error );
          });

        } else {
          q.reject({
            data: {
              errors: [ {detail: "Person cannot be found"} ]
            }
          });
        }
      } else {
        q.reject({
          data: {
            errors:[ {detail: "Person cannot be found"} ]
          }
        });
      }

      return q.promise;

    }


    /**
     * Finds a person id from the prefetched memberships data list.
     *
     */

    function findPersonId( membershipsMainData ){
      for(var i = 0; i < membershipsMainData.length; i++){
        if( membershipsMainData[i].relationships.organization.data.id === appService.getCompanyId() ){
          return membershipsMainData[i].relationships.person.data.id;
        }
      }
    }


    /**
     * Finds a person by id from the prefetched memberships included list.
     *
     */

    function findPersonById( personId, membershipsIncludedData ){
      for(var i = 0; i < membershipsIncludedData.length; i++){
        if( membershipsIncludedData[i].id === personId ){
          return membershipsIncludedData[i].attributes;
        }
      }
    }


    /**
     * Returns a date in a "YYYY-mm-dd" format.
     *
     */

    function formatDate( date )  {
      if( !(date instanceof Date) ){
        date = new Date( date )
      }
      var incrementMonth = date.getMonth() + 1;
      var day  = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
      var month = incrementMonth > 9 ? incrementMonth : "0" + incrementMonth;
      var year  = date.getFullYear();

      return [ year,  month, day ].join("-");
    }



    /**
     * Substract number of days from new date instance.
     *
     */

    function substractDaysFromDate( days, date ) {
      var date = date;
      if( !(date instanceof Date) ){
        return null;
      }
      date = new Date( date.getTime() );
      return date.setDate( date.getDate() - days );
    }




    /**
     * Add number of days to a new date instance.
     *
     */

    function addDaysToDate( days, date ) {
      var date = date;
      if( !(date instanceof Date) ){
        return null;
      }
      date = new Date( date.getTime() );
      return date.setDate( date.getDate() + days );
    }



    /**
     * Request for deletion via service layer, and if
     * delete is successful remove time entry from view model.
     *
     */

    function deleteTimeEntry( $event, timeEntry ){

      $event.preventDefault();
      // show loader indicator
      showTimetrackerLoaderIndicator();

      timetrackerService.deleteTimeEntry( headers, timeEntry.id ).then(function(response){
        if(vm.errors.length > 0) {
          vm.errors = [];
        }

        if(response.status === 204){
          vm.timeEntries = vm.timeEntries.filter(function(item){
            if(item.id !== timeEntry.id){
              return item;
            }
          })
        }
      }).catch(function(error){
        if(error && error.data && error.data.errors.length > 0){
          vm.errors = error.data.errors;
        }
      }).finally(function(){
        hideTimetrackerLoaderIndicator();
      });
    }


    /**
     * Request for creation of an entry via service layer, and if
     * creating is successful push that item to the list of time entries in the view model.
     *
     */

    function createTimeEntry($event){

      var buttonEl = $event.target;
      var defaultButtonText = buttonEl.innerHTML;
      $event.preventDefault();

      vm.hiddenDismissable = false;
      vm.dismissed = false;

      headers["Content-Type"] = "application/vnd.api+json";
      if( isNaN(vm.time) ){
        vm.errors.timeEntries = [ {detail: "Please provide numeric value for the time field"} ];
        return;
      }

      // show loader indicator
      showTimetrackerLoaderIndicator();
      buttonEl.innerHTML = "Creating...";
      buttonEl.disabled = true;

      var requestData =  {
        type: "time_entries",
        attributes: {
          note: vm.note,
          date: vm.today,
          time: vm.time
        },
        relationships: {
          person: {
            data: {
              type: "people",
              id: personId
            }
          },
          service: {
            data: {
              type: "services",
              id: vm.service.id
            }
          }
      }};

      timetrackerService.createTimeEntry( headers, { data: requestData } ).then(function(services){
        vm.timeEntries.push(services.data.data);
        if(vm.errors.timeEntries && vm.errors.timeEntries.length > 0) {
          vm.errors.timeEntries = [];
        }
      }).catch(function(error){
        if(error && error.data && error.data.errors.length > 0){
          vm.errors.timeEntries = error.data.errors;
        }
      }).finally(function(){
        hideTimetrackerLoaderIndicator();
        vm.hiddenDismissable = true;
        vm.dismissed = true;
        buttonEl.innerHTML = defaultButtonText;
        buttonEl.disabled = false;
      });

    }



    function hideLoaderIndicator(){
      $rootScope.loading = false;
      $timeout(function(){
        $rootScope.hideLoader = true;
      }, 500);
    }


    function hideTimetrackerLoaderIndicator(){
      vm.loadingTimetracker = false;
      $timeout(function(){
        vm.hideTimetrackerLoader = true;
      }, 100);
    }


    function showTimetrackerLoaderIndicator(){
      vm.loadingTimetracker = true;
      vm.hideTimetrackerLoader = false;
    }

    function anyErrors(){
      for(n in vm.errors){
        for( var i = 0; vm.errors[n] && i < vm.errors[n].length; i++){
          if( vm.errors[n][i] ){
            return true;
          }
        }
      }
      return false;
    }


    function dismiss(){
      vm.dismissed = true;
      $timeout(function(){
        vm.hiddenDismissable = true;
      }, 500);
    }

  }






}());
