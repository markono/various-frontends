(function(){
  'use-strict';

  /**
   * Timetracker module.
   * Include App module as a dependency.
   *
   */

  angular.module('app.timetracker', ['app']);

}());
