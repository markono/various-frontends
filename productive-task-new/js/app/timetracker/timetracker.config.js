(function(){
  'use-strict';

  /**
   * Main route for the app, timetracker screen.
   * Since there is no login procedure, we are getting required auth data
   * for REST communication via App's service layer.
   *
   */

  angular
      .module('app.timetracker')
      .config(config);

  config.inject = ["$stateProvider", "$httpProvider"];

  function config($stateProvider, $httpProvider) {

      $stateProvider
          .state('app.timetracker', {
              url: "/timetracker",
              templateUrl: 'js/app/timetracker/timetracker.html',
              controller: 'TimetrackerController',
              controllerAs: 'vm',
              resolve: {
                /**
                 * Prepare headers for timetracker controller.
                 */
                 headers:  function(appService){
                   var companyId = appService.getCompanyId();
                   var userToken = appService.getUserToken();
                   return {
                       'X-Auth-Token': userToken,
                       'X-Organization-Id': companyId
                   };
                 }
               }
          });

  }

}());
