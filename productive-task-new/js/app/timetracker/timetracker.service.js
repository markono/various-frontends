(function(){
  'use-strict';

  /**
   * CRUD methods for common timetracker operations.
   *
   */

  angular
      .module('app.timetracker')
      .factory('timetrackerService', timetrackerService);

  timetrackerService.$inject = ['$http', '$q', "AppConstants"];

  function timetrackerService($http, $q, AppConstants) {
      var appConstants  = AppConstants();

      return {
          fetchOrganizationMemberships: fetchOrganizationMemberships,
          fetchTimeEntriesByFilters: fetchTimeEntriesByFilters,
          fetchServices: fetchServices,
          createTimeEntry: createTimeEntry,
          deleteTimeEntry: deleteTimeEntry
      };


      ///////////////////////////////
      ///// METHOD DEFINITIONS //////
      ///////////////////////////////

      function fetchOrganizationMemberships( headers ) {
          var q = $q.defer();
          $http({
            method: 'GET',
            url: appConstants.api.memberships,
            timeout: 10000,
            headers: headers
          }).then(function(response) {
            q.resolve(response);
          }).catch(function(error) {
            q.reject(error);
          });
          return q.promise;
      }

      function fetchTimeEntriesByFilters( headers, filters ) {
          var q = $q.defer();
          $http({
            method: 'GET',
            url: appConstants.api.time_entries,
            timeout: 10000,
            headers: headers,
            params: {
              "filter[person_id]": filters.personId,
              "filter[after]": filters.after,
              "filter[before]": filters.before
            }
          }).then(function(response) {
            q.resolve(response);
          }).catch(function(error) {
            q.reject(error);
          });
          return q.promise;
      }

      function fetchServices( headers ) {
          var q = $q.defer();
          $http({
            method: 'GET',
            url: appConstants.api.services,
            timeout: 10000,
            headers: headers
          }).then(function(response) {
            q.resolve(response);
          }).catch(function(error) {
            q.reject(error);
          });
          return q.promise;
      }

      function createTimeEntry( headers, timeEntry ) {
          var q = $q.defer();
          $http({
            method: 'POST',
            url: appConstants.api.time_entries,
            timeout: 10000,
            headers: headers,
            data: timeEntry
          }).then(function(response) {
            q.resolve(response);
          }).catch(function(error) {
            q.reject(error);
          });
          return q.promise;
      }


      function deleteTimeEntry( headers, id ) {
          var q = $q.defer();
          $http({
            method: 'DELETE',
            url: appConstants.api.time_entries + "/" + id,
            headers: headers,
            timeout: 10000
          }).then(function(response) {
            q.resolve(response);
          }).catch(function(error) {
            q.reject(error);
          });
          return q.promise;
      }



  }


}());
