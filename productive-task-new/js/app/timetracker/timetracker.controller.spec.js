describe('TimetrackerController: ', function() {
  beforeEach(module('app.login'));

  var $controller, $scope, appService, $state,timetrackerService, $httpBackend, commonResponse, $rootScope;

  beforeEach(inject(function(_$controller_,_$rootScope_,_appService_, _$state_,_timetrackerService_,_$httpBackend_){
    $controller = _$controller_;
    $scope = _$rootScope_.$new();
    $rootScope = _$rootScope_;
    appService = _appService_;
    $state = _$state_;
    timetrackerService = _timetrackerService_;
    $httpBackend = _$httpBackend_;
    commonMockedResponse = {
      data: {
        data: [{
          relationships: {
            organization: {
              data: {
                id: "1494"
              }
            },
            person: {
              data: {
                id: "1494"
              }
            }
          }
        }],
        included: [{
          id: "1494",
          attributes: {
            organization: {
              data: {
                id: "1494"
              }
            },
            person: {
              data: {
                id: "1494"
              }
            }
          }
        }]
      }
    };


  }));






  it("Should succesfully render Create new item section and Time Entries list and remove loading indicator", function() {

    spyOn(timetrackerService, 'fetchOrganizationMemberships').and.callFake(function(){
      return {
        then:function(fn){
          fn(commonMockedResponse);
          return {
            then:function(fn){
              fn(commonMockedResponse);
            },
            catch:function(fn){
              return {}
            }
          }
        },
        catch:function(fn){
          return {}
        }
      }
    });


    spyOn(timetrackerService, 'fetchServices').and.callFake(function(){
      return {
        then:function(fn){
          fn(commonMockedResponse);
          return {
            then:function(fn){
              fn(commonMockedResponse);
            },
            catch:function(fn){
              return {}
            }
          }
        },
        catch:function(fn){
          return {}
        }

      }
    });


    spyOn(timetrackerService, 'fetchTimeEntriesByFilters').and.callFake(function(){
      return {
        then:function(fn){
          fn(commonMockedResponse);
          return {
            then:function(fn){
              fn(commonMockedResponse);
            },
            catch:function(fn){}
          }
        }
      }
    });

    var controller = $controller('TimetrackerController', {headers: {"test":1}});
    $scope.$apply();
    // list of time entries
    expect(controller.timeEntries.length).toEqual(1);
    // one service/project item
    expect(controller.service).toEqual(jasmine.objectContaining({relationships: {
      organization: {
        data: {
          id: "1494"
        }
      },
      person: {
        data: {
          id: "1494"
        }
      }
    }}));
    expect($rootScope.loading).toEqual(false);


  });







  it("Should not render Create new item section and Time Entries list due to server error  and should remove loading indicator", function() {

    spyOn(timetrackerService, 'fetchOrganizationMemberships').and.callFake(function(){
      return {
        then:function(fn){
          //fn(commonMockedResponse);
          return {
            then:function(fn){
              //fn(commonMockedResponse);
            },
            catch:function(fn){
              fn({data: {errors: [{ error: 1}]}});
            }
          }
        },
        catch:function(fn){
          //fn(commonMockedResponse);
        }
      }
    });


    spyOn(timetrackerService, 'fetchServices').and.callFake(function(){
      return {
        then:function(fn){
          //fn(commonMockedResponse);
          return {
            then:function(fn){
              //fn(commonMockedResponse);
            },
            catch:function(fn){
              fn({data: {errors: [{ error: 1}]}});
            }
          }
        },
        catch:function(fn){
          //fn(commonMockedResponse);
        }

      }
    });


    spyOn(timetrackerService, 'fetchTimeEntriesByFilters').and.callFake(function(){
      return {
        then:function(fn){
        //  fn(commonMockedResponse);
          return {
            then:function(fn){
            //  fn(commonMockedResponse);
            },
            catch:function(fn){
              fn({data: {errors: [{ error: 1}]}});
            }
          }
        }
      }
    });

    var controller = $controller('TimetrackerController', {headers: {"test":1}});
    $scope.$apply();
    // list of time entries
    expect(controller.timeEntries.length).toEqual(0);
    // one service/project item
    expect(controller.service).not.toEqual(jasmine.objectContaining({relationships: {
      organization: {
        data: {
          id: "1494"
        }
      },
      person: {
        data: {
          id: "1494"
        }
      }
    }}));
    expect(controller.errors.timeEntries.length).toEqual(1);
    expect(controller.errors.services.length).toEqual(1);
    expect($rootScope.loading).toEqual(false);


  });






  it("Date should be 2017-03-17", function() {

    spyOn(timetrackerService, 'fetchOrganizationMemberships').and.callFake(function(){
      return {
        then:function(fn){
          fn(commonMockedResponse);
          return {
            then:function(fn){
              fn(commonMockedResponse);
            },
            catch:function(fn){
              return {}
            }
          }
        },
        catch:function(fn){
          return {}
        }
      }
    });


    spyOn(timetrackerService, 'fetchServices').and.callFake(function(){
      return {
        then:function(fn){
          fn(commonMockedResponse);
          return {
            then:function(fn){
              fn(commonMockedResponse);
            },
            catch:function(fn){
              return {}
            }
          }
        },
        catch:function(fn){
          return {}
        }

      }
    });


    spyOn(timetrackerService, 'fetchTimeEntriesByFilters').and.callFake(function(){
      return {
        then:function(fn){
          fn(commonMockedResponse);
          return {
            then:function(fn){
              fn(commonMockedResponse);
            },
            catch:function(fn){}
          }
        }
      }
    });


    var controller = $controller('TimetrackerController', {headers: {"test":1}});
    var date = controller.formatDate(new Date(2017,2,17));
    $scope.$apply();
    expect(date).toEqual("2017-03-17");



  });



it("Date should be substracted by 8 days", function() {

  spyOn(timetrackerService, 'fetchOrganizationMemberships').and.callFake(function(){
    return {
      then:function(fn){
        fn(commonMockedResponse);
        return {
          then:function(fn){
            fn(commonMockedResponse);
          },
          catch:function(fn){
            return {}
          }
        }
      },
      catch:function(fn){
        return {}
      }
    }
  });


  spyOn(timetrackerService, 'fetchServices').and.callFake(function(){
    return {
      then:function(fn){
        fn(commonMockedResponse);
        return {
          then:function(fn){
            fn(commonMockedResponse);
          },
          catch:function(fn){
            return {}
          }
        }
      },
      catch:function(fn){
        return {}
      }

    }
  });


  spyOn(timetrackerService, 'fetchTimeEntriesByFilters').and.callFake(function(){
    return {
      then:function(fn){
        fn(commonMockedResponse);
        return {
          then:function(fn){
            fn(commonMockedResponse);
          },
          catch:function(fn){}
        }
      }
    }
  });


  var controller = $controller('TimetrackerController', {headers: {"test":1}});
  var date = controller.substractDaysFromDate(8, new Date(2017,2,17));
  $scope.$apply();
  expect(date).toEqual(new Date(2017,2,9).getTime());



});






it("10 days should be added to a date", function() {

  spyOn(timetrackerService, 'fetchOrganizationMemberships').and.callFake(function(){
    return {
      then:function(fn){
        fn(commonMockedResponse);
        return {
          then:function(fn){
            fn(commonMockedResponse);
          },
          catch:function(fn){
            return {}
          }
        }
      },
      catch:function(fn){
        return {}
      }
    }
  });


  spyOn(timetrackerService, 'fetchServices').and.callFake(function(){
    return {
      then:function(fn){
        fn(commonMockedResponse);
        return {
          then:function(fn){
            fn(commonMockedResponse);
          },
          catch:function(fn){
            return {}
          }
        }
      },
      catch:function(fn){
        return {}
      }

    }
  });


  spyOn(timetrackerService, 'fetchTimeEntriesByFilters').and.callFake(function(){
    return {
      then:function(fn){
        fn(commonMockedResponse);
        return {
          then:function(fn){
            fn(commonMockedResponse);
          },
          catch:function(fn){}
        }
      }
    }
  });


  var controller = $controller('TimetrackerController', {headers: {"test":1}});
  var date = controller.addDaysToDate(10, new Date(2017,2,10));
  $scope.$apply();
  expect(date).toEqual(new Date(2017,2,20).getTime());



});



});
